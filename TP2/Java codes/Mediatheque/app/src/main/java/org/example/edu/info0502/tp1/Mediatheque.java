// Mediatheque.java
package edu.info0502.tp1;

import java.util.ArrayList;

public class Mediatheque {
    private String nomProprietaire;
    private ArrayList<Media> collectionMedias;

    public Mediatheque() {
        this.nomProprietaire = "";
        this.collectionMedias = new ArrayList<>();
    }

    public Mediatheque(String nomProprietaire, ArrayList<Media> collectionMedias) {
        this.nomProprietaire = nomProprietaire;
        this.collectionMedias = collectionMedias;
    }

    public void add(Media media) {
        collectionMedias.add(media);
    }

    @Override
    public String toString() {
        return "Médiathèque appartenant à " + nomProprietaire + ", Collection: " + collectionMedias;
    }
}
