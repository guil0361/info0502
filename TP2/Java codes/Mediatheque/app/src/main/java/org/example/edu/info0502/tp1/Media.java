// Media.java
package edu.info0502.tp1;

public class Media {
    private String titre;
    private StringBuffer cote;
    private int note;

    private static String nomMediatheque;

    public Media() { 
        this.titre = ""; 
        this.cote = new StringBuffer(); 
        this.note = 0; 
    }
    
    public Media(String titre, StringBuffer cote, int note) {
        this.titre = titre;
        this.cote = cote;
        this.note = note;
    }

    public Media(Media autre) {
        this.titre = autre.titre;
        this.cote = new StringBuffer(autre.cote);
        this.note = autre.note;
    }

    public String getTitre() { 
        return titre; 
    }
     public void setTitre(String titre) {
         this.titre = titre; 
    }
    
    public StringBuffer getCote() {
         return new StringBuffer(cote); 
    }
    public void setCote(StringBuffer cote) {
         this.cote = cote; 
    }
    
    public int getNote() { 
        return note; 
    }
    public void setNote(int note) { 
        this.note = note; 
    }

    public static String getNomMediatheque() { 
        return nomMediatheque; 
    }
    public static void setNomMediatheque(String nom) {
         nomMediatheque = nom; 
    }

    @Override
    public String toString() {
        return "Titre: " + titre + ", Cote: " + cote + ", Note: " + note;
    }
}
