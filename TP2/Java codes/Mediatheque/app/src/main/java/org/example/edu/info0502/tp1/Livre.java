// Livre.java
package edu.info0502.tp1;

public class Livre extends Media {
    private String auteur; 
    private String ISBN;

    public Livre(String titre, StringBuffer cote, int note, String auteur, String ISBN) {
        super(titre, cote, note);
        this.auteur = auteur;
        this.ISBN = ISBN;
    }

    public String getAuteur() { return auteur; }
    public void setAuteur(String auteur) { this.auteur = auteur; }

    public String getISBN() { return ISBN; }
    public void setISBN(String ISBN) { this.ISBN = ISBN; }

    @Override
    public String toString() {
        return super.toString() + ", Auteur: " + auteur + ", ISBN: " + ISBN;
    }
}
