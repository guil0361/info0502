package org.example;

public class App {
    public static void main(String[] args) {
        System.out.println("Lorenzo - INFO0502: Introduction à la programmation répartie!");
    }

    // Add this method to satisfy the test
    public String getGreeting() {
        return "Hello, world!";
    }
}
