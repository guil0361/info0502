package edu.info0502.poker;

public class Card {
    private String suit;  // e.g., hearts, diamonds, clubs, spades
    private String rank;  // e.g., 2, 3, 4, ..., J, Q, K, A

    public Card(String suit, String rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public String getSuit() {
        return suit;
    }

    public String getRank() {
        return rank;
    }

    @Override
    public String toString() {
        return rank + " of " + suit;
    }
}
