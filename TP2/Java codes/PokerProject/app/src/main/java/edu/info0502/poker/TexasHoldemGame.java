package edu.info0502.poker;

import java.util.ArrayList;
import java.util.List;

public class TexasHoldemGame {
    private Deck deck;
    private List<Card> communityCards;  // 5 community cards
    private List<Player> players;       // List of players

    public static void main(String[] args) {
        TexasHoldemGame game = new TexasHoldemGame(4);  // 4 players
        game.playGame();
    }

    public TexasHoldemGame(int numberOfPlayers) {
        deck = new Deck();
        deck.shuffle();

        communityCards = new ArrayList<>();
        players = new ArrayList<>();
        for (int i = 0; i < numberOfPlayers; i++) {
            players.add(new Player());
        }
    }

    public void dealHoleCards() {
        for (Player player : players) {
            player.receiveCard(deck.dealCard());
            player.receiveCard(deck.dealCard());
        }
    }

    public void dealCommunityCards() {
        for (int i = 0; i < 5; i++) {
            communityCards.add(deck.dealCard());
        }
    }

    public List<Card> getCommunityCards() {
        return communityCards;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void displayGameState() {
        System.out.println("Community Cards: " + communityCards);
        for (int i = 0; i < players.size(); i++) {
            System.out.println("Player " + (i + 1) + "'s hand: " + players.get(i).getHoleCards());
        }
    }

    public void playGame() {
        dealHoleCards();
        dealCommunityCards();
        displayGameState();
        // TODO: Implement hand evaluation and determine the winner
    }
}
