package edu.info0502.poker;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private List<Card> holeCards;  // Player's private cards

    public Player() {
        holeCards = new ArrayList<>(2);
    }

    public void receiveCard(Card card) {
        if (holeCards.size() < 2) {
            holeCards.add(card);
        }
    }

    public List<Card> getHoleCards() {
        return holeCards;
    }

    @Override
    public String toString() {
        return "Player's hand: " + holeCards;
    }
}
