package tp5.edu.info0502.tp5.publisher;

import org.eclipse.paho.client.mqttv3.*;
import tp5.edu.info0502.tp5.shared.Message;

import java.util.Scanner;

public class PokerPublisher {
    private static final String BROKER_URL = "tcp://localhost:1883";
    private static final String CLIENT_ID = "PokerPublisher";

    public static void main(String[] args) {
        try {
            MqttClient client = new MqttClient(BROKER_URL, CLIENT_ID);
            client.connect();

            System.out.println("Publisher connected to MQTT broker!");

            Scanner scanner = new Scanner(System.in);

            while (true) {
                System.out.println("Enter topic:");
                String topic = scanner.nextLine();
                System.out.println("Enter message:");
                String message = scanner.nextLine();

                sendMessage(client, topic, message);
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private static void sendMessage(MqttClient client, String topic, String content) throws MqttException {
        Message message = new Message(topic, content);
        MqttMessage mqttMessage = new MqttMessage(message.toJson().getBytes());
        client.publish(topic, mqttMessage);
        System.out.println("Message sent: " + content);
    }
}
