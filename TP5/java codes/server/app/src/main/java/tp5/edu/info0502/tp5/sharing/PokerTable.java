package tp5.edu.info0502.tp5.shared;

import java.util.ArrayList;
import java.util.List;

public class PokerTable {
    private String tableId;
    private List<String> players;
    private int maxPlayers;
    private boolean isActive;

    public PokerTable(String tableId, int maxPlayers) {
        this.tableId = tableId;
        this.players = new ArrayList<>();
        this.maxPlayers = maxPlayers;
        this.isActive = true;
    }

    public String getTableId() {
        return tableId;
    }

    public List<String> getPlayers() {
        return players;
    }

    public boolean addPlayer(String playerId) {
        if (players.size() < maxPlayers && isActive) {
            players.add(playerId);
            return true;
        }
        return false;
    }

    public boolean removePlayer(String playerId) {
        return players.remove(playerId);
    }

    public void closeTable() {
        isActive = false;
    }

    public boolean isActive() {
        return isActive;
    }
}
