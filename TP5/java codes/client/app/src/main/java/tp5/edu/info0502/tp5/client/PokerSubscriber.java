package tp5.edu.info0502.tp5.subscriber;

import org.eclipse.paho.client.mqttv3.*;
import tp5.edu.info0502.tp5.shared.Message;

public class PokerSubscriber {
    private static final String BROKER_URL = "tcp://localhost:1883";
    private static final String CLIENT_ID = "PokerSubscriber";

    public static void main(String[] args) {
        try {
            MqttClient client = new MqttClient(BROKER_URL, CLIENT_ID);
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    System.err.println("Connection lost: " + cause.getMessage());
                }

                @Override
                public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                    String content = new String(mqttMessage.getPayload());
                    Message message = Message.fromJson(content);
                    System.out.println("Message received on topic [" + topic + "]: " + message.getContent());
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    // No-op for subscribers
                }
            });

            client.connect();
            System.out.println("Subscriber connected to MQTT broker!");

            // Subscribe to all poker-related topics
            client.subscribe("poker/#");
            System.out.println("Subscribed to topics: poker/#");

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
