// Main.java
public class Main {
    public static void main(String[] args) {
        Mediatheque mediatheque = new Mediatheque();
        Livre livre = new Livre("It", new StringBuffer("123ABC"), 5, "Stephen King", "978-1234534890");
        Film film = new Film("Sweeney Todd: The Demon Barber of Fleet Street", new StringBuffer("987XYZ"), 9, "Someone",2007 );

        mediatheque.add(livre);
        mediatheque.add(film);

        System.out.println(mediatheque);
    }
}
