package tp4.edu.info0502.tp4.consumer;

import com.rabbitmq.client.*;
import tp4.edu.info0502.tp4.shared.Question;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QCMServer {
    private static final String QUEUE_REQUEST = "QCM_Requests";
    private static final String QUEUE_RESPONSE = "QCM_Responses";

    public static void main(String[] args) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
                channel.queueDeclare(QUEUE_REQUEST, false, false, false, null);
                channel.queueDeclare(QUEUE_RESPONSE, false, false, false, null);

                // Predefined questions
                List<Question> questions = new ArrayList<>();
                questions.add(new Question("What is the capital of France?", new String[]{"Paris", "Berlin", "Madrid"}, "Paris"));

                DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                    String userName = new String(delivery.getBody(), "UTF-8");
                    System.out.println("User connected: " + userName);

                    // Respond with a question
                    String questionMessage = "Question: " + questions.get(0).getQuestion();
                    channel.basicPublish("", QUEUE_RESPONSE, null, questionMessage.getBytes());
                };

                channel.basicConsume(QUEUE_REQUEST, true, deliverCallback, consumerTag -> {});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
