package tp4.edu.info0502.tp4.shared;

import java.io.Serializable;

public class User implements Serializable {
    private String userId;
    private String userName;

    public User(String userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }
}
