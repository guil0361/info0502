package tp4.edu.info0502.tp4.shared;

import java.io.Serializable;

public class Answer implements Serializable {
    private String questionId;
    private String userAnswer;

    public Answer(String questionId, String userAnswer) {
        this.questionId = questionId;
        this.userAnswer = userAnswer;
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getUserAnswer() {
        return userAnswer;
    }
}
