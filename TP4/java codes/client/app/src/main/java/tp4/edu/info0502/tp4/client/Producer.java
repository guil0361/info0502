package tp4.edu.info0502.tp4.producer;

import com.rabbitmq.client.*;
import tp4.edu.info0502.tp4.shared.Answer;
import tp4.edu.info0502.tp4.shared.User;

import java.io.IOException;
import java.util.Scanner;

public class QCMClient {
    private static final String QUEUE_REQUEST = "QCM_Requests";
    private static final String QUEUE_RESPONSE = "QCM_Responses";

    public static void main(String[] args) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
                channel.queueDeclare(QUEUE_REQUEST, false, false, false, null);
                channel.queueDeclare(QUEUE_RESPONSE, false, false, false, null);

                Scanner scanner = new Scanner(System.in);
                System.out.println("Enter your user ID:");
                String userId = scanner.nextLine();
                System.out.println("Enter your username:");
                String userName = scanner.nextLine();

                User user = new User(userId, userName);
                channel.basicPublish("", QUEUE_REQUEST, null, user.getUserName().getBytes());
                System.out.println("User registered: " + user.getUserName());

                // Listen for responses
                DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                    String message = new String(delivery.getBody(), "UTF-8");
                    System.out.println("Response from server: " + message);
                };

                channel.basicConsume(QUEUE_RESPONSE, true, deliverCallback, consumerTag -> {});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
