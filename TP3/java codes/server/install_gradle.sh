#!/bin/bash

# Update and upgrade system packages
echo "Updating system packages..."
sudo apt update && sudo apt upgrade -y

# Install Java
echo "Installing Java (OpenJDK 17)..."
sudo apt install -y openjdk-17-jdk

# Verify Java installation
echo "Verifying Java installation..."
java -version
if [ $? -ne 0 ]; then
    echo "Java installation failed. Exiting."
    exit 1
fi

# Set JAVA_HOME environment variable
echo "Setting JAVA_HOME environment variable..."
JAVA_HOME=$(dirname $(dirname $(readlink -f $(which java))))
echo "export JAVA_HOME=$JAVA_HOME" >> ~/.bashrc
echo "export PATH=$JAVA_HOME/bin:$PATH" >> ~/.bashrc
source ~/.bashrc

# Verify JAVA_HOME
echo "JAVA_HOME is set to: $JAVA_HOME"
if [ -z "$JAVA_HOME" ]; then
    echo "JAVA_HOME is not set correctly. Exiting."
    exit 1
fi

# Install Gradle
echo "Installing Gradle..."
sudo apt install -y wget unzip
wget https://services.gradle.org/distributions/gradle-8.4-bin.zip -P /tmp
sudo unzip -d /opt/gradle /tmp/gradle-8.4-bin.zip

# Set GRADLE_HOME environment variable
echo "Setting GRADLE_HOME environment variable..."
echo "export GRADLE_HOME=/opt/gradle/gradle-8.4" >> ~/.bashrc
echo "export PATH=$GRADLE_HOME/bin:$PATH" >> ~/.bashrc
source ~/.bashrc

# Verify Gradle installation
echo "Verifying Gradle installation..."
gradle --version
if [ $? -ne 0 ]; then
    echo "Gradle installation failed. Exiting."
    exit 1
fi

# Success message
echo "Java and Gradle are successfully installed and configured!"
echo "Java version:"
java -version
echo "Gradle version:"
gradle --version
