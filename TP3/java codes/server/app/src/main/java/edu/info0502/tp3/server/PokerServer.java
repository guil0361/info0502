package edu.info0502.tp3.server;

import edu.info0502.tp3.shared.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import edu.info0502.tp3.shared.Hand;
import edu.info0502.tp3.shared.Player;
import edu.info0502.tp3.shared.Deck;
import edu.info0502.tp3.shared.Card;


public class PokerServer {
    private final int port;
    private List<ClientHandler> clients;
    private boolean gameInProgress;
    private Hand communityCards;

    public PokerServer(int port) {
        this.port = port;
        this.clients = new ArrayList<>();
        this.gameInProgress = false;
    }

    public static void main(String[] args) {
        PokerServer server = new PokerServer(12345);
        server.start();
    }

    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            System.out.println("Server running on port " + port);
            while (true) {
                Socket clientSocket = serverSocket.accept();
                if (clients.size() < 10) {
                    ClientHandler clientHandler = new ClientHandler(clientSocket, this);
                    clients.add(clientHandler);
                    new Thread(clientHandler).start();
                } else {
                    System.out.println("Connection refused: table is full.");
                    clientSocket.close();
                }
            }
        } catch (IOException e) {
            System.err.println("Server error: " + e.getMessage());
        }
    }

    public synchronized boolean isNicknameAvailable(String nickname) {
        return clients.stream().noneMatch(c -> c.getPlayer().getName().equals(nickname));
    }

    public synchronized void removeClient(ClientHandler clientHandler) {
        clients.remove(clientHandler);
        System.out.println("Player disconnected: " + clientHandler.getPlayer().getName());
    }

    public synchronized void startGame() {
        if (clients.size() < 2) {
            System.out.println("Not enough players to start the game.");
            return;
        }
        if (gameInProgress) {
            System.out.println("Game already in progress.");
            return;
        }

        gameInProgress = true;
        Deck deck = new Deck(1);
        deck.shuffle();

        for (ClientHandler client : clients) {
            Card card1 = deck.drawCard();
            Card card2 = deck.drawCard();
            client.getPlayer().addHiddenCard(card1);
            client.getPlayer().addHiddenCard(card2);
            client.sendMessage(new Message(Message.TypeMessage.CARD_DISTRIBUTION, card1));
            client.sendMessage(new Message(Message.TypeMessage.CARD_DISTRIBUTION, card2));
        }

        communityCards = new Hand(deck, 3); // Flop
        communityCards.addCard(deck.drawCard()); // Turn
        communityCards.addCard(deck.drawCard()); // River

        for (Card card : communityCards.getCards()) {
            broadcast(new Message(Message.TypeMessage.FLOP, card));
        }

        calculateAndSendResults();
    }

    private void calculateAndSendResults() {
        List<Hand> hands = new ArrayList<>();
        for (ClientHandler client : clients) {
            Hand playerHand = new Hand(communityCards);
            playerHand.addCards(client.getPlayer().getHiddenCards());
            hands.add(playerHand);
        }

        Hand bestHand = hands.get(0);
        int winnerIndex = 0;
        for (int i = 1; i < hands.size(); i++) {
            if (hands.get(i).compareHands(bestHand)) {
                bestHand = hands.get(i);
                winnerIndex = i;
            }
        }

        StringBuilder results = new StringBuilder();
        for (int i = 0; i < clients.size(); i++) {
            results.append("Player ").append(clients.get(i).getPlayer().getName()).append(": ")
                    .append(hands.get(i).evaluateCombination()).append("\n");
        }
        broadcast(new Message(Message.TypeMessage.RESULTS, results.toString()));

        String winner = clients.get(winnerIndex).getPlayer().getName();
        broadcast(new Message(Message.TypeMessage.GAME_RESULT, "Winner: " + winner));

        gameInProgress = false;
    }

    public synchronized void broadcast(Message message) {
        for (ClientHandler client : clients) {
            client.sendMessage(message);
        }
    }
}
