package edu.info0502.tp3.shared;

import java.io.Serializable;

public class Card implements Serializable {
    private String rank;
    private String suit;

    public Card(String rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    @Override
    public String toString() {
        return rank + " of " + suit;
    }
}
