package edu.info0502.tp3.server;

import edu.info0502.tp3.shared.*;
import java.io.*;
import java.net.Socket;
import edu.info0502.tp3.shared.Player;
import edu.info0502.tp3.shared.Message;
import edu.info0502.tp3.shared.Card;


public class ClientHandler implements Runnable {
    private final Socket socket;
    private final PokerServer server;
    private Player player;
    private ObjectInputStream input;
    private ObjectOutputStream output;

    public ClientHandler(Socket socket, PokerServer server) {
        this.socket = socket;
        this.server = server;
        try {
            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            System.err.println("Error initializing streams: " + e.getMessage());
        }
    }

    public Player getPlayer() {
        return player;
    }

    public void sendMessage(Message message) {
        try {
            output.writeObject(message);
            output.flush();
        } catch (IOException e) {
            System.err.println("Error sending message: " + e.getMessage());
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                Message message = (Message) input.readObject();
                // Handle messages from client
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Client disconnected.");
        }
    }
}
