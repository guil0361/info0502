package edu.info0502.tp3.shared;

import java.io.Serializable;

public class Message implements Serializable {
    public enum TypeMessage {
        AUTH, AUTH_SUCC, AUTH_FAIL,
        CARD_DISTRIBUTION, FLOP, TURN, RIVER,
        RESULTS, GAME_RESULT, ERROR, DISCONNECT
    }

    private TypeMessage type;
    private Object content;

    public Message(TypeMessage type, Object content) {
        this.type = type;
        this.content = content;
    }

    public TypeMessage getType() {
        return type;
    }

    public Object getContent() {
        return content;
    }
}
