package edu.info0502.tp3.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Hand implements Serializable {
    private List<Card> cards;

    public Hand() {
        cards = new ArrayList<>();
    }

    // Add this constructor to initialize with an existing hand and extra cards
    public Hand(Deck deck, int numCards) {
        this();
        for (int i = 0; i < numCards; i++) {
            cards.add(deck.drawCard());
        }
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public List<Card> getCards() {
        return cards;
    }

    public boolean compareHands(Hand otherHand) {
        // Compare hands logic (stubbed for now)
        return this.cards.size() > otherHand.cards.size();
    }

    public String evaluateCombination() {
        // Evaluation logic (stubbed for now)
        return "Sample Combination";
    }

    public Hand(Hand otherHand) {
    this();
    this.cards.addAll(otherHand.getCards());
}

    public void addCards(List<Card> cardsToAdd) {
    cards.addAll(cardsToAdd);
}


}
