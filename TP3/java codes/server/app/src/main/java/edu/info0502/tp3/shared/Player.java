package edu.info0502.tp3.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Player implements Serializable {
    private String name;
    private List<Card> hiddenCards;

    public Player(String name) {
        this.name = name;
        this.hiddenCards = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Card> getHiddenCards() {
        return hiddenCards;
    }

    public void addHiddenCard(Card card) {
        hiddenCards.add(card);
    }
}
