package tp3.edu.info0502.tp3.shared;

import java.io.Serializable;

public class Message implements Serializable {
    public enum TypeMessage {
        AUTH, START_POKER, DISCONNECT, CARD_DISTRIBUTION,
        FLOP, TURN, RIVER, RESULTS, GAME_RESULT, ERROR
    }

    private TypeMessage type;
    private Object content;

    public Message(TypeMessage type, Object content) {
        this.type = type;
        this.content = content;
    }

    public TypeMessage getType() {
        return type;
    }

    public Object getContent() {
        return content;
    }
}
