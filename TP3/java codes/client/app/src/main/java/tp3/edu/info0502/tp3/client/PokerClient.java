package tp3.edu.info0502.tp3.client;

import tp3.edu.info0502.tp3.shared.Message;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class PokerClient {
    private Socket socket;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private String serverAddress = "127.0.0.1";
    private int serverPort = 12345;

    public static void main(String[] args) {
        PokerClient client = new PokerClient();
        client.start();
    }

    public void start() {
        try {
            socket = new Socket(serverAddress, serverPort);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
            System.out.println("Connected to the server!");

            new Thread(this::listenToServer).start();
            handleUserInput();

        } catch (IOException e) {
            System.err.println("Connection error: " + e.getMessage());
        }
    }

    private void handleUserInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a nickname:");
        String nickname = scanner.nextLine();

        try {
            outputStream.writeObject(new Message(Message.TypeMessage.AUTH, nickname));
            outputStream.flush();

            while (true) {
                System.out.println("Commands: START -> Start Game | EXIT -> Disconnect");
                String command = scanner.nextLine();

                switch (command.toUpperCase()) {
                    case "START":
                        outputStream.writeObject(new Message(Message.TypeMessage.START_POKER, null));
                        outputStream.flush();
                        break;
                    case "EXIT":
                        outputStream.writeObject(new Message(Message.TypeMessage.DISCONNECT, null));
                        outputStream.flush();
                        socket.close();
                        return;
                    default:
                        System.out.println("Invalid command.");
                }
            }
        } catch (IOException e) {
            System.err.println("Error sending message: " + e.getMessage());
        }
    }

    private void listenToServer() {
        try {
            while (true) {
                Message message = (Message) inputStream.readObject();
                if (message == null) break;

                switch (message.getType()) {
                    case CARD_DISTRIBUTION -> System.out.println("Card: " + message.getContent());
                    case RESULTS -> System.out.println("Results: " + message.getContent());
                    case ERROR -> System.err.println("Error: " + message.getContent());
                    default -> System.out.println("Server: " + message.getContent());
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Disconnected from server.");
        }
    }
}
